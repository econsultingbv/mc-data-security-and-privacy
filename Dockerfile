FROM python:3

WORKDIR /usr/src/app

RUN pip install cryptography
RUN pip install pillow
RUN pip install pyaes
RUN pip install pbkdf2
