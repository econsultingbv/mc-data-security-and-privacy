# Preparation
Please complete the following steps before the Masterclass Data Security and Privacy.

## Necessary
The following software needs to be installed and configured before the Masterclass.  

- Install Burp Suite (Community Edition 2023.10.2.3)
- Install Docker Desktop

## Run Anonimatron
Perform the following steps to run anonimatron:

- Clone the GIT repository https://gitlab.com/econsultingbv/mc-data-security-and-privacy.git
- cd anonimatron
- Run anonimatron.sh or anonimator.bat (depending on whether you're using a Linux/Mac or a Windows laptop)
- Check the output:

```
2023-10-27 12:31:06,030 INFO  (Configuration.java:0) Configuration read from /usr/src/config.xml []
Configuration does not contain <table> or <file> elements. Nothing done.
```

If you see an error, you probably haven't installed docker correctly or docker is not available on your path. Make sure Docker Desktop is installed.

## Background material
The following articles are a nice warming up for the Masterclass: 

- https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610
- https://medium.com/@jsoverson/exploiting-developer-infrastructure-is-insanely-easy-9849937e81d4
- https://citizenlab.ca/2022/01/cross-country-exposure-analysis-my2022-olympics-app
- https://www.youtube.com/watch?v=JIo-V0beaBw

Feel free to share other articles, blogs and video's about these subjects with the other members in your talent class.

