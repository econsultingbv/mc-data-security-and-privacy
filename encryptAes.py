import pyaes, pbkdf2, binascii, os, secrets
from PIL import Image
from cryptography.fernet import Fernet


def createEncryptionKey():
    # Derive a 256-bit AES encryption key from the password
    password = "s3cr3t*c0d3"
    passwordSalt = os.urandom(16)
    return pbkdf2.PBKDF2(password, passwordSalt).read(32)


def encryptWithAESmodeECB(key, data) -> bytes:
    return data


def encryptWithAESmodeCBC(key, data) -> bytes:
    return data


def encryptWithFernet(key, data) -> bytes:
    return data


key = createEncryptionKey()
print('AES encryption key:', binascii.hexlify(key))

image = Image.open('encryption/tux.png').convert('RGBA').convert('RGB')
data = image.tobytes()

ecbCipher = encryptWithAESmodeECB(key, data)
cbcCipher = encryptWithAESmodeCBC(key, data)
fernetCipher = encryptWithFernet(Fernet.generate_key(), data)

Image.frombytes("RGB", image.size, ecbCipher, "raw", "RGB").save('encryption/tux.ecb.png')
Image.frombytes("RGB", image.size, cbcCipher, "raw", "RGB").save('encryption/tux.cbc.png')
Image.frombytes("RGB", image.size, fernetCipher, "raw", "RGB").save('encryption/tux.fernet.png')

print('Encryption finished')
